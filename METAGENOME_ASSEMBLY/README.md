## SOFTWARE ABSTRACT

MEGAHIT is a metagenomic assembler used for assembling large and complex metagenomics Next Generation Sequencing (NGS) reads. It uses succint de Bruijn graph(sdBG) to achieve time and cost efficient assembly. NGS technologies has enabled massive sequencing of organisms providing the opportunities to study metagenomics and understand natural micriobiota like human guts. With lack of reference genome to the enormous sets of sequences produced, the need for de novo assembly of metagenomics data is crucial. The process for de novo assembly is costly especially when working with large datasets. For instance, a typical soil metagenomics dataset comprises of 252G base even after trimming low quality bases (Li et al., 2015). The megahit utilizes the sdGB and parallelism using GPUs to make the process of metagenomics assembly faster.

## INSTALLATION

Installing the MEGAHIT program in HPCC can be done in three different methods. I will explain each method;


1. The first methods involves building the software from the sources following the following steps;
		
		git clone https://github.com/voutcn/megahit.git     //cloning the data into the current directory.
	
		cd megahit     // the cd to the megahit directory and run the commands below

		mkdir build && cd build 	//making the build directory and cd to it.

		cmake .. -DCMAKE_BUILD_TYPE=Release  # add -DCMAKE_INSTALL_PREFIX=MY_PREFIX if needed

		make -j4
			
	After the command is done running;
		
		cd ../../ to go back to the directory with the submission script and submit the job to scheduler using

		sbatch megahit_src.sb

	Using this method one might take a bit long time especially doing the make -j4 command.


2. The second method makes use of already compiled binaries.

	To download pre-built binaries for Pre-built binaries for x86_64 Linux use the command below;

		wget https://github.com/voutcn/megahit/releases/download/v1.2.9/MEGAHIT-1.2.9-Linux-x86_64-static.tar.gz

	The unzip the files from the downloaded file archive using the command below

		tar zvxf MEGAHIT-1.2.9-Linux-x86_64-static.tar.gz

	Then run the submission script to submit job to scheduler

		sbatch megahit_bin.sb

3. The fourth methods make use of already install MEGAHIT program in HPCC. I just realized the software was in HPCC while looking at the module list command:

	To load the megahit use the module load MEGAHIT command or directy use
		
		sbatch megahit.sb    which will load the module and submit the job to scheduler


## EXAMPLE DATA;

The example data is found in METAGENOME_ASSEMBLY under a data folder. The data used is for Campylobacter jejuni. You can run the code using any of submission script according to the method you used for install the software to you director. For each method the command for submitting the code to slurm scheduler is indicated below:

	For method 1: use the submission script below

		sbatch megahit_src.sb

	For method 2: use the submission script below
		
		sbatch megahit_src.sb

	For method 3: use the submission script below

		sbatch megahit.sb

And before each submission, ensure you are in the directory wherethe submission script is.

After running the sbatch and is done running, the contigs are found in result_directory. The print statements indicating with informations of k-mers being used by sbDG is found in megahit_err_8.

References:

Li, D., Liu, C.-M., Luo, R., Sadakane, K., & Lam, T.-W. (2015). MEGAHIT: an ultra-fast single-node solution for large and 	complex metagenomics assembly via succinct de Bruijn graph. Bioinformatics, 31(10), 1674–1676. https://doi.org/10.1093/bioinformatics/btv033
